# jal_codehighlighting_with_gtksourceview

Code highlighting with gtksourceview for Xed and Gedit

## Intro

This is an attempt to provide source code highlighting with XED and GEDIT (Linux) editors for programs and libraries written in JAL programming language for Microchip 8-bits PIC micros (in combination with the JalV2 compiler and Jallib libraries).

See also 
- http://www.justanotherlanguage.org 
- https://github.com/jallib/jallib

This language highlighting file follows the documentation of the JalV2 compiler only. Although the compiler is frequently used in combination with Jallib libraries there is no highlighting of commonly used keywords and constants with Jallib files. So for example TRUE, FALSE, HIGH, LOW, etc. are not highlighted. 


**Installation:** 

Put the file jal.lang in one of these directories:

- ~/.local/share/gtksourceview-4/language-specs 
- ~/.local/share/gtksourceview-3.0/language-specs

depending on which is the newest version of GtkSourceView in your Linux system.

When the library doesn't exist create it first, e.g. in a terminal session with:

    mkdir -p ~/.local/share/gtksourceview-4/language-specs 

That's all!

Recommended theme: Cobalt! Choose as follows:

- Xed menubar select Edit -> Preferences -> Theme > Cobalt
- Gedit menubar select settings -> Preferences -> Font and Colors -> Cobalt

See the file example_jal_highlighting.jpg for the result.

Of course you may have other preferences!



## License
GNU Lesser General Public License




